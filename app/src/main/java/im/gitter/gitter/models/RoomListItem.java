package im.gitter.gitter.models;

@Deprecated
public interface RoomListItem {
    String getUri();
    String getAvatarUrl(int size);
    String getName();
    boolean hasActivity();
    int getUnreadCount();
    int getMentionCount();
    String getRoomId();
}
