package im.gitter.gitter.network;

import android.content.Context;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Room;

@Deprecated
public class DeprecatedRoomRequest extends ApiRequest<Room> {

    public DeprecatedRoomRequest(Context context, String path, Response.Listener<Room> listener, Response.ErrorListener errorListener) {
        super(context, path, listener, errorListener);
    }

    public DeprecatedRoomRequest(Context context, int method, String path, JSONObject jsonRequest, Response.Listener<Room> listener, Response.ErrorListener errorListener) {
        super(context, method, path, jsonRequest, listener, errorListener);
    }

    @Override
    protected Room parseJsonInBackground(String json) throws JSONException {
        return new ModelFactory().createRoom(new JSONObject(json));
    }
}
