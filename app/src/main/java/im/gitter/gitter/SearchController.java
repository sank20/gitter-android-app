package im.gitter.gitter;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.RoomListItem;
import im.gitter.gitter.models.User;
import im.gitter.gitter.network.Api;
import im.gitter.gitter.network.VolleySingleton;

public class SearchController {

    private final RequestQueue requestQueue;
    private final Listener listener;
    private final ModelFactory modelFactory = new ModelFactory();
    private ArrayList<Room> roomResults = null;
    private ArrayList<User> userResults = null;
    private Api api;

    public SearchController(Context context, Listener listener) {
        this.api = new Api(context);
        this.requestQueue = VolleySingleton.getInstance(context).getRequestQueue();
        this.listener = listener;
    }

    public void search(String term) {
        String urlTerm = term;
        try {
            urlTerm = URLEncoder.encode(term, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        cancelPendingSearch();

        if (term.length() > 0) {
            requestQueue.add(api.createJsonObjRequest(
                    Request.Method.GET,
                    "/v1/rooms?q=" + urlTerm,
                    createRoomSearchListener(),
                    createFailureListener()
            ).setTag(this));

            requestQueue.add(api.createJsonObjRequest(
                    Request.Method.GET,
                    "/v1/user?type=gitter&q=" + urlTerm,
                    createUserSearchListener(),
                    createFailureListener()
            ).setTag(this));
        }
    }

    public void clear() {
        cancelPendingSearch();
        listener.onResults(new ArrayList<RoomListItem>());
    }

    private void cancelPendingSearch() {
        requestQueue.cancelAll(this);
        userResults = null;
        roomResults = null;
    }

    private Response.Listener<JSONObject> createRoomSearchListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject body) {
                JSONArray roomsJson = null;
                ArrayList<Room> rooms = new ArrayList<>();

                try {
                    roomsJson = body.getJSONArray("results");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(roomsJson != null) {
                    for (int i=0; i < roomsJson.length(); i++) {
                        try {
                            rooms.add(modelFactory.createRoom(roomsJson.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                roomResults = rooms;
                showResultsIfComplete();
            }
        };
    }

    private Response.Listener<JSONObject> createUserSearchListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject body) {
                JSONArray usersJson = null;
                ArrayList<User> users = new ArrayList<>();

                try {
                    usersJson = body.getJSONArray("results");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(usersJson != null) {
                    for (int i=0; i < usersJson.length(); i++) {
                        try {
                            User user = modelFactory.createUser(usersJson.getJSONObject(i));
                            users.add(user);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                userResults = users;
                showResultsIfComplete();
            }
        };
    }

    private void showResultsIfComplete() {
        if (roomResults != null && userResults != null) {
            List<RoomListItem> results = new ArrayList<>();

            for (Room room : roomResults) {
                results.add(room);
            }

            for (User user : userResults) {
                results.add(user);
            }

            Collections.sort(results, new RoomListItemComparator());
            listener.onResults(results);
        }
    }

    private Response.ErrorListener createFailureListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if one request fails, they should all fail
                cancelPendingSearch();
                listener.onError();
            }
        };
    }

    private class RoomListItemComparator implements Comparator<RoomListItem> {
        @Override
        public int compare(RoomListItem lhs, RoomListItem rhs) {
            return lhs.getUri().length() - rhs.getUri().length();
        }
    }

    public interface Listener {
        void onResults(List<RoomListItem> results);
        void onError();
    }
}
