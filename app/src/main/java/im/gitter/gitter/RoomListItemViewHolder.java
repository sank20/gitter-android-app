package im.gitter.gitter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import im.gitter.gitter.models.RoomListItem;
import im.gitter.gitter.utils.CircleNetworkImageView;

public class RoomListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private PositionClickListener listener;
    CircleNetworkImageView avatarView;
    TextView nameView;
    TextView unreadView;

    public RoomListItemViewHolder(View itemView, PositionClickListener listener) {
        super(itemView);
        this.listener = listener;
        avatarView = (CircleNetworkImageView) itemView.findViewById(R.id.avatar);
        nameView = (TextView) itemView.findViewById(R.id.title);
        unreadView = (TextView) itemView.findViewById(R.id.badge);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onClick(getAdapterPosition());
    }

    public static void bind(RoomListItemViewHolder roomViewHolder, RoomListItem roomListItem, ImageLoader imageLoader, int avatarSize, Drawable mentionBadge, Drawable unreadBadge, Drawable activityBadge) {
        roomViewHolder.avatarView.setImageUrl(roomListItem.getAvatarUrl(avatarSize), imageLoader);
        roomViewHolder.avatarView.setDefaultImageResId(R.drawable.default_avatar);
        roomViewHolder.avatarView.setErrorImageResId(R.drawable.default_avatar);

        roomViewHolder.nameView.setText(roomListItem.getName());

        roomViewHolder.unreadView.setVisibility(roomListItem.hasActivity() ? View.VISIBLE : View.GONE);

        int mentions = roomListItem.getMentionCount();
        int unreadItems = roomListItem.getUnreadCount();

        if (mentions > 0) {
            roomViewHolder.unreadView.setText("@");
            roomViewHolder.unreadView.setBackground(mentionBadge);
        } else if (unreadItems > 0) {
            roomViewHolder.unreadView.setText(unreadItems > 99 ? "99+" : String.valueOf(unreadItems));
            roomViewHolder.unreadView.setBackground(unreadBadge);
        } else {
            roomViewHolder.unreadView.setText("");
            roomViewHolder.unreadView.setBackground(activityBadge);
        }
    }
}
