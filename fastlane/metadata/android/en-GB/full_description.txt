Gitter is where developers come to talk. We provide free public/private chat rooms for developer communities, open source projects, technical teams, and businesses.

KEY FEATURES
- Unlimited public/private chat rooms
- Unlimited and searchable chat history
- Unlimited integrations
- Built on top of GitHub, the world’s largest network of software developers

The app itself is open source and published/deployed via GitLab CI (merge requests welcome), https://gitlab.com/gitlab-org/gitter/gitter-android-app

TRUSTED BY
Gitter is home to over 30,000 developer communities, including The .NET Foundation, Google Material Design, Angular.js, Backbone, Node.js, Scala, The W3C and many more.

PROBLEMS? FEEDBACK?
The more you tell us, the better Gitter gets. In case of any questions, and in order to find out more about the product, visit our docs: https://gitlab.com/gitlab-org/gitter/webapp/tree/develop/docs
You can also give us direct feedback in the Gitter HQ channel: https://gitter.im/gitterHQ/gitter
Let us know how we can improve! Issues welcome, https://gitlab.com/gitlab-org/gitter/gitter-android-app/issues
